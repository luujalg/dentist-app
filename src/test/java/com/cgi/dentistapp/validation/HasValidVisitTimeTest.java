package com.cgi.dentistapp.validation;

import com.cgi.dentistapp.dto.DentistVisitDto;
import com.cgi.dentistapp.entity.Dentist;
import com.cgi.dentistapp.service.DentistService;
import com.cgi.dentistapp.service.DentistVisitService;
import org.junit.Before;
import org.junit.Test;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.TemporalAdjusters;
import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class HasValidVisitTimeTest {

  private ValidationRule validationRule;
  private DentistVisitService mockedDentistVisitService;
  private final long DENTIST_ID = 0L;
  private final long VISIT_ID = 0L;

  @Before
  public void setUp() {
    DentistService mockedDentistService = mock(DentistService.class);
    mockedDentistVisitService = mock(DentistVisitService.class);
    validationRule = new HasValidVisitTime(mockedDentistVisitService, mockedDentistService);
    when(mockedDentistService.getDentistsById()).thenReturn(createDentistMap());
    when(mockedDentistVisitService.findVisit(VISIT_ID)).thenReturn(createVisit());
  }

  @Test
  public void registrationWithNoTimeSetHasErrors() {
    DentistVisitDto dto = DentistVisitDto.builder()
        .date(getNextMonday(LocalDate.now()))
        .dentistId(DENTIST_ID)
        .build();

    ValidationResult validationResult = new ValidationResult();
    validationRule.apply(dto, validationResult);
    assertTrue(validationResult.hasErrors());
  }

  @Test
  public void registrationTodayButTimeInThePastHasErrors() {
    DentistVisitDto dto = DentistVisitDto.builder()
        .date(LocalDate.now())
        .time(LocalTime.now().minusHours(1L))
        .dentistId(DENTIST_ID)
        .build();

    ValidationResult validationResult = new ValidationResult();
    validationRule.apply(dto, validationResult);
    assertTrue(validationResult.hasErrors());
  }

  @Test
  public void registrationTodayTimeWithinTheHourHasErrors() {
    DentistVisitDto dto = DentistVisitDto.builder()
        .date(LocalDate.now())
        .time(LocalTime.now().plusMinutes(30L))
        .dentistId(DENTIST_ID)
        .build();

    ValidationResult validationResult = new ValidationResult();
    validationRule.apply(dto, validationResult);
    assertTrue(validationResult.hasErrors());
  }

  @Test
  public void updatingSavedVisitWithSameDateAndTimeHasNoErrors() {
    DentistVisitDto dto = createVisit();
    ValidationResult validationResult = new ValidationResult();
    validationRule.apply(dto, validationResult);
    assertFalse(validationResult.hasErrors());
  }

  @Test
  public void updatingSavedVisitWithDifferentValidDateAndTimeHasNoErrors() {
    DentistVisitDto dto = DentistVisitDto.builder()
        .id(VISIT_ID)
        .dentistId(DENTIST_ID)
        .date(getNextMonday(LocalDate.now()).plusDays(1L))
        .time(LocalTime.of(10, 0))
        .build();

    when(mockedDentistVisitService.findAvailableTimes(dto.getDentistId(), dto.getDate()))
        .thenReturn(singletonList(dto.getTime()));
    ValidationResult validationResult = new ValidationResult();
    validationRule.apply(dto, validationResult);
    assertFalse(validationResult.hasErrors());
  }

  @Test
  public void updatingSavedVisitWithUnavailableDateAndTimeHasErrors() {
    DentistVisitDto dto = DentistVisitDto.builder()
        .id(VISIT_ID)
        .dentistId(DENTIST_ID)
        .date(getNextMonday(LocalDate.now()).plusDays(1L))
        .time(LocalTime.of(10, 0))
        .build();

    when(mockedDentistVisitService.findAvailableTimes(dto.getDentistId(), dto.getDate()))
        .thenReturn(singletonList(dto.getTime().plusHours(1L)));
    ValidationResult validationResult = new ValidationResult();
    validationRule.apply(dto, validationResult);
    assertTrue(validationResult.hasErrors());
  }

  @Test
  public void newRegistrationWithUnavailableDateAndTimeHasErrors() {
    DentistVisitDto dto = DentistVisitDto.builder()
        .dentistId(DENTIST_ID)
        .date(getNextMonday(LocalDate.now()).plusDays(1L))
        .time(LocalTime.of(10, 0))
        .build();

    when(mockedDentistVisitService.findAvailableTimes(dto.getDentistId(), dto.getDate()))
        .thenReturn(singletonList(dto.getTime().plusHours(1L)));
    ValidationResult validationResult = new ValidationResult();
    validationRule.apply(dto, validationResult);
    assertTrue(validationResult.hasErrors());
  }

  @Test
  public void newRegistrationWithAvailableDateAndTimeHasNoErrors() {
    DentistVisitDto dto = DentistVisitDto.builder()
        .dentistId(DENTIST_ID)
        .date(getNextMonday(LocalDate.now()).plusDays(1L))
        .time(LocalTime.of(10, 0))
        .build();

    when(mockedDentistVisitService.findAvailableTimes(dto.getDentistId(), dto.getDate()))
        .thenReturn(singletonList(dto.getTime()));
    ValidationResult validationResult = new ValidationResult();
    validationRule.apply(dto, validationResult);
    assertFalse(validationResult.hasErrors());
  }

  @Test
  public void validRegistrationWithNoAvailableTimesHasErrors() {
    DentistVisitDto dto = DentistVisitDto.builder()
        .dentistId(DENTIST_ID)
        .date(getNextMonday(LocalDate.now()).plusDays(1L))
        .time(LocalTime.of(10, 0))
        .build();

    when(mockedDentistVisitService.findAvailableTimes(dto.getDentistId(), dto.getDate()))
        .thenReturn(emptyList());
    ValidationResult validationResult = new ValidationResult();
    validationRule.apply(dto, validationResult);
    assertTrue(validationResult.hasErrors());
  }

  private DentistVisitDto createVisit() {
    return DentistVisitDto.builder()
        .id(VISIT_ID)
        .dentistId(DENTIST_ID)
        .date(getNextMonday(LocalDate.now()))
        .time(LocalTime.of(10, 0))
        .editable(true)
        .build();
  }

  private Map<Long, Dentist> createDentistMap() {
    Map<Long, Dentist> dentists = new HashMap<>();
    Dentist dentist = Dentist.builder()
        .id(DENTIST_ID)
        .build();
    dentists.put(DENTIST_ID, dentist);
    return dentists;
  }

  private LocalDate getNextMonday(LocalDate date) {
    return date.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
  }

}