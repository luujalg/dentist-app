package com.cgi.dentistapp.validation;

import com.cgi.dentistapp.dto.DentistVisitDto;
import com.cgi.dentistapp.entity.Dentist;
import com.cgi.dentistapp.service.DentistService;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class HasValidDentistTest {

  private ValidationRule validationRule;

  @Before
  public void setUp() {
    DentistService mockedService = mock(DentistService.class);
    validationRule = new HasValidDentist(mockedService);
    when(mockedService.getDentistsById()).thenReturn(createDentistMap());
  }

  @Test
  public void validDentistIdHasNoErrors() {
    DentistVisitDto dto = createDto(0L);
    ValidationResult validationResult = new ValidationResult();
    validationRule.apply(dto, validationResult);
    assertFalse(validationResult.hasErrors());
  }

  @Test
  public void nonExistingDentistIdHasErrors() {
    DentistVisitDto dto = createDto(10L);
    ValidationResult validationResult = new ValidationResult();
    validationRule.apply(dto, validationResult);
    assertTrue(validationResult.hasErrors());
  }

  private Map<Long, Dentist> createDentistMap() {
    Map<Long, Dentist> dentists = new HashMap<>();
    for (long i = 0; i < 3; i++) {
      Dentist dentist = Dentist.builder()
          .id(i)
          .build();
      dentists.put(i, dentist);
    }
    return dentists;
  }

  private DentistVisitDto createDto(long dentistId) {
    return DentistVisitDto.builder()
        .dentistId(dentistId)
        .build();
  }

}