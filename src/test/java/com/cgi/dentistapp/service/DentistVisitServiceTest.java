package com.cgi.dentistapp.service;

import com.cgi.dentistapp.dto.DentistVisitDto;
import com.cgi.dentistapp.entity.DentistVisit;
import com.cgi.dentistapp.repository.DentistVisitDao;
import org.junit.Before;
import org.junit.Test;

import java.sql.Date;
import java.time.*;
import java.time.temporal.TemporalAdjusters;
import java.util.List;

import static com.cgi.dentistapp.service.DentistVisitService.*;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DentistVisitServiceTest {

  private DentistVisitDao visitRepository;
  private DentistVisitService visitService;
  private final long VISIT_ID = 0L;
  private final long DENTIST_ID = 0L;
  private final ZoneId DEFAULT_ZONE_ID = ZoneId.systemDefault();

  @Before
  public void setUp() {
    visitRepository = mock(DentistVisitDao.class);
    visitService = new DentistVisitService(visitRepository);
  }

  @Test
  public void registrationInThePastIsNotEditable() {
    when(visitRepository.findOne(VISIT_ID)).thenReturn(createVisit(true));
    DentistVisitDto dto = visitService.findVisit(VISIT_ID);
    assertFalse(dto.isEditable());
  }

  @Test
  public void registrationInTheFutureIsEditable() {
    when(visitRepository.findOne(VISIT_ID)).thenReturn(createVisit(false));
    DentistVisitDto dto = visitService.findVisit(VISIT_ID);
    assertTrue(dto.isEditable());
  }

  @Test
  public void registrationTooSoonInTheFutureIsNotEditable() {
    LocalDate date = LocalDate.now();
    LocalTime time = LocalTime.now()
        .plusHours(HOURS_BEFORE_VISIT_TO_ALLOW_EDIT_DELETE)
        .minusMinutes(30L);

    when(visitRepository.findOne(VISIT_ID)).thenReturn(createVisit(date, time));
    DentistVisitDto dto = visitService.findVisit(VISIT_ID);
    assertFalse(dto.isEditable());
  }

  @Test
  public void availableTimesDoesNotContainRegisteredTimes() {
    LocalDate date = LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.MONDAY));
    LocalTime time = LocalTime.of(10, 0);
    when(visitRepository.findVisits(DENTIST_ID, date)).thenReturn(singletonList(createVisit(date, time)));
    List<LocalTime> availableTimes = visitService.findAvailableTimes(DENTIST_ID, date);
    assertFalse(availableTimes.contains(time));
  }

  @Test
  public void availableTimesDoesNotContainTimesInThePastIfDateIsToday() {
    LocalDate date = LocalDate.now();
    LocalTime time = TIME_OF_FIRST_ALLOWED_VISIT.plusMinutes(VISIT_TIME_DURATION);
    when(visitRepository.findVisits(DENTIST_ID, date)).thenReturn(emptyList());
    List<LocalTime> availableTimes = visitService.findAvailableTimes(DENTIST_ID, date);
    assertFalse(availableTimes.contains(time));
  }

  @Test
  public void availableTimesDoesNotContainTimesWithinTheHourIfDateIsToday() {
    LocalDate date = LocalDate.now();
    when(visitRepository.findVisits(DENTIST_ID, date)).thenReturn(emptyList());
    List<LocalTime> availableTimes = visitService.findAvailableTimes(DENTIST_ID, date);
    assertFalse(availableTimes.contains(TIME_OF_FIRST_ALLOWED_VISIT.plusHours(1L)));
  }

  private DentistVisit createVisit(boolean isPastRegistration) {
    LocalDate date = LocalDate.now().plusDays(isPastRegistration ? -1L : 1L);
    LocalTime time = LocalTime.of(10, 0);
    return createVisit(date, time);
  }

  private DentistVisit createVisit(LocalDate date, LocalTime time) {
    LocalDateTime dateTime = LocalDateTime.of(date, time);
    return new DentistVisit(DENTIST_ID, Date.from(dateTime.atZone(DEFAULT_ZONE_ID).toInstant()));
  }

}