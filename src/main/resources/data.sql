INSERT INTO dentist (first_name, last_name) VALUES
  ('Andrus', 'Tamm'),
  ('Margit', 'Sepp'),
  ('Joosep', 'Ploomipuu');

INSERT INTO dentist_visit (dentist_id, visit_date) VALUES
    (1, '2021-02-10 10:00:00.0'),
    (2, '2021-02-10 11:00:00.0'),
    (2, '2021-02-01 12:00:00.0'),
    (2, '2021-02-01 13:00:00.0');