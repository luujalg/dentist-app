package com.cgi.dentistapp.controller;

import com.cgi.dentistapp.dto.DentistVisitDto;
import com.cgi.dentistapp.dto.SearchFilterDto;
import com.cgi.dentistapp.entity.Dentist;
import com.cgi.dentistapp.service.DentistService;
import com.cgi.dentistapp.service.DentistVisitService;
import com.cgi.dentistapp.service.DentistVisitValidationService;
import com.cgi.dentistapp.validation.ValidationResult;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.validation.Valid;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

@Controller
@EnableAutoConfiguration
@AllArgsConstructor
public class DentistAppController extends WebMvcConfigurerAdapter {

    private final DentistVisitService visitService;
    private final DentistService dentistService;
    private final DentistVisitValidationService validationService;

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/results").setViewName("results");
    }

    @GetMapping("/")
    public String showRegisterForm(DentistVisitDto dentistVisitDto, Model model) {
        model.addAttribute("dentists", dentistService.getAllDentists());
        return "form";
    }

    @GetMapping("/{id}")
    public String viewRegistration(@PathVariable Long id, DentistVisitDto dto, Model model) {
        setDtoToSavedValues(dto, id);
        if (dto.getId() == null) {
            return "redirect:/form";
        }

        List<LocalTime> availableTimes = visitService.findAvailableTimes(dto.getDentistId(), dto.getDate());
        availableTimes.add(dto.getTime());

        model.addAttribute("visit", dto);
        model.addAttribute("isReadOnly", true);
        Map<Long, Dentist> dentistsById = dentistService.getDentistsById();
        model.addAttribute("dentistsById", dentistsById);
        model.addAttribute("dentists", dentistsById.values());
        model.addAttribute("availableTimes", availableTimes.stream().sorted().collect(toList()));

        return "form";
    }

    @PostMapping("/")
    public String postRegisterForm(@Valid DentistVisitDto dto, BindingResult bindingResult, Model model) {
        ValidationResult validationResult = validationService.performOnSaveValidation(dto);
        if (bindingResult.hasErrors() || validationResult.hasErrors()) {
            addValidationErrorsToView(validationResult, bindingResult);
            model.addAttribute("isReadOnly", false);
            model.addAttribute("dentists", dentistService.getAllDentists());
            model.addAttribute("availableTimes",
                visitService.findAvailableTimes(dto.getDentistId(), dto.getDate()));
            return "form";
        }

        visitService.addVisit(dto);
        return "redirect:/results";
    }

    @GetMapping("/registrations")
    public String showRegistrations(SearchFilterDto searchFilterDto, BindingResult bindingResult, Model model) {
        addRegistrationsViewAttributes(model);
        return "registrations";
    }

    @PostMapping("/availableTimes")
    public String findAvailableTimes(@Valid DentistVisitDto dto, BindingResult bindingResult, Model model) {
        model.addAttribute("isEdit", dto.getId() != null);
        model.addAttribute("isReadOnly", false);
        model.addAttribute("dentists", dentistService.getAllDentists());

        ValidationResult validationResult = validationService.performPreSaveValidation(dto);
        if (bindingResult.hasErrors() || validationResult.hasErrors()) {
            addValidationErrorsToView(validationResult, bindingResult);
            return "form";
        }

        model.addAttribute("availableTimes",
            visitService.findAvailableTimes(dto.getDentistId(), dto.getDate()));
        return "form";
    }

    @DeleteMapping("/{id}/delete")
    public String deleteRegistration(@PathVariable Long id,
                                     SearchFilterDto searchFilterDto, BindingResult bindingResult) {
        visitService.deleteRegistration(id);
        return "redirect:/registrations";
    }

    @GetMapping("/results")
    public String showResults() {
        return "results";
    }

    @GetMapping("/{id}/edit")
    public String editVisit(@PathVariable Long id, DentistVisitDto dto, Model model) {
        setDtoToSavedValues(dto, id);
        if (dto.getId() == null) {
            return "redirect:/form";
        }

        List<LocalTime> availableTimes = visitService.findAvailableTimes(dto.getDentistId(), dto.getDate());
        availableTimes.add(dto.getTime());

        model.addAttribute("isEdit", true);
        model.addAttribute("isReadOnly", false);
        model.addAttribute("dentists", dentistService.getAllDentists());
        model.addAttribute("availableTimes", availableTimes.stream().sorted().collect(toList()));

        return "form";
    }

    @PostMapping("/{id}/update")
    public String update(@PathVariable Long id, @Valid DentistVisitDto dto, BindingResult bindingResult, Model model) {
        if (!validationService.isEditable(id)) {
            throw new IllegalArgumentException("Can't edit dentist visit (ID: " + id + ")");
        }

        ValidationResult validationResult = validationService.performOnSaveValidation(dto);
        if (bindingResult.hasErrors() || validationResult.hasErrors()) {
            addValidationErrorsToView(validationResult, bindingResult);
            model.addAttribute("dentists", dentistService.getAllDentists());
            return "form";
        }

        visitService.updateVisit(dto);
        return "redirect:/results";
    }

    @PostMapping("/search")
    public String findRegistrations(@Valid SearchFilterDto searchFilterDto, BindingResult bindingResult, Model model) {
        Map<Long, Dentist> dentists = dentistService.getDentistsById();
        model.addAttribute("dentistsById", dentists);
        model.addAttribute("dentists", dentists.values());

        if (bindingResult.hasErrors()) {
            return "registrations";
        }

        model.addAttribute("registrations", visitService.findVisits(searchFilterDto));

        return "registrations";
    }

    private void addRegistrationsViewAttributes(Model model) {
        Map<Long, Dentist> dentists = dentistService.getDentistsById();
        model.addAttribute("dentistsById", dentists);
        model.addAttribute("dentists", dentists.values());
        model.addAttribute("registrations", visitService.getAllRegistrations());
    }

    private void setDtoToSavedValues(DentistVisitDto dto, Long id) {
        DentistVisitDto savedVisit = visitService.findVisit(id);

        if (savedVisit == null) {
            return;
        }

        dto.setId(savedVisit.getId());
        dto.setDentistId(savedVisit.getDentistId());
        dto.setDate(savedVisit.getDate());
        dto.setTime(savedVisit.getTime());
        dto.setEditable(savedVisit.isEditable());
        dto.setVisitDate(savedVisit.getVisitDate());
    }

    private void addValidationErrorsToView(ValidationResult validationResult, BindingResult bindingResult) {
        validationResult.getFieldErrors().forEach(bindingResult::rejectValue);
        validationResult.getGlobalErrors().forEach((k, v) -> bindingResult.reject(k, v, "registration.nok"));
    }

}
