package com.cgi.dentistapp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class SearchFilterDto {

  private List<Long> dentistIds;

  @DateTimeFormat(pattern = "dd.MM.yyyy")
  private LocalDate startDate;

  @DateTimeFormat(pattern = "dd.MM.yyyy")
  private LocalDate endDate;

  @Override
  public String toString() {
    return String.format("SearchFilterDto: [dentistIds=%s, startDate=%s, endDate=%s]", dentistIds, startDate, endDate);
  }

}
