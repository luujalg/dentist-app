package com.cgi.dentistapp.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class DentistVisitDto {

    Long id;

    @NotNull
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    LocalDate date;

    @DateTimeFormat(pattern = "HH:mm")
    LocalTime time;

    @NotNull
    Long dentistId;

    boolean editable;
    Date visitDate;

    @Override
    public String toString() {
        return String.format("DentistVisitDto: [id=%d, date=%s, time=%s, dentistId=%d]", id, date, time, dentistId);
    }
}
