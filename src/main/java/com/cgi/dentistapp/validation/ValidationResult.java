package com.cgi.dentistapp.validation;

import java.util.HashMap;
import java.util.Map;

public class ValidationResult {

  private final Map<String, String> fieldErrors;
  private final Map<String, Object[]> globalErrors;

  public ValidationResult() {
    fieldErrors = new HashMap<>();
    globalErrors = new HashMap<>();
  }

  public void addFieldError(String field, String errorCode) {
    fieldErrors.put(field, errorCode);
  }

  public void addGlobalError(String errorCode, Object[] args) {
    globalErrors.put(errorCode, args);
  }

  public Map<String, String> getFieldErrors() {
    return fieldErrors;
  }

  public Map<String, Object[]> getGlobalErrors() {
    return globalErrors;
  }

  public boolean hasErrors() {
    return !fieldErrors.isEmpty() || !globalErrors.isEmpty();
  }

}
