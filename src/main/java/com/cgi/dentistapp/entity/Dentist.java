package com.cgi.dentistapp.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "dentist")
public class Dentist {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Size(min = 1, max = 250)
  private String firstName;

  @Size(min = 1, max = 250)
  private String lastName;

  public String getFullName() {
    return String.format("%s %s", firstName, lastName);
  }

  public Long getId() {
    return id;
  }
}
