package com.cgi.dentistapp.repository;

import com.cgi.dentistapp.entity.DentistVisit;
import org.springframework.data.repository.CrudRepository;

public interface DentistVisitDao extends CrudRepository<DentistVisit, Long>, DentistVisitFinder {
}
