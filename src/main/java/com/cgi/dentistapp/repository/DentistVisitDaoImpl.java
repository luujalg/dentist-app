package com.cgi.dentistapp.repository;

import com.cgi.dentistapp.dto.SearchFilterDto;
import com.cgi.dentistapp.entity.DentistVisit;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.util.Collections.emptyList;

public class DentistVisitDaoImpl implements DentistVisitFinder {

  private final ZoneId DEFAULT_ZONE_ID = ZoneId.systemDefault();

  @PersistenceContext
  private EntityManager entityManager;

  @Override
  public List<DentistVisit> findVisits(SearchFilterDto dto) {
    CriteriaBuilder builder = entityManager.getCriteriaBuilder();
    CriteriaQuery<DentistVisit> query = builder.createQuery(DentistVisit.class);
    Root<DentistVisit> root = query.from(DentistVisit.class);
    query.select(root);

    List<Predicate> predicates = new ArrayList<>();
    if (!dto.getDentistIds().isEmpty()) {
      CriteriaBuilder.In<Long> inClause = builder.in(root.get("dentistId"));
      dto.getDentistIds().forEach(inClause::value);
      predicates.add(inClause);
    }

    Path<Date> visitDate = root.get("visitDate");
    if (dto.getStartDate() != null) {
      predicates.add(builder.greaterThanOrEqualTo(visitDate, getStartOrEndOfDate(dto.getStartDate(), true)));
    }

    if (dto.getEndDate() != null) {
      predicates.add(builder.lessThanOrEqualTo(visitDate, getStartOrEndOfDate(dto.getEndDate(), false)));
    }

    query.where(builder.and(predicates.toArray(new Predicate[0])));

    return entityManager.createQuery(query).getResultList();
  }

  @Override
  public List<DentistVisit> findVisits(Long dentistId, LocalDate date) {
    if (dentistId == null || date == null) {
      return emptyList();
    }

    CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
    CriteriaQuery<DentistVisit> query = criteriaBuilder.createQuery(DentistVisit.class);
    Root<DentistVisit> root = query.from(DentistVisit.class);

    List<Predicate> predicates = new ArrayList<>();
    Path<Date> visitDate = root.get("visitDate");
    predicates.add(criteriaBuilder.equal(root.get("dentistId"), dentistId));
    predicates.add(criteriaBuilder.greaterThanOrEqualTo(visitDate, getStartOrEndOfDate(date, true)));
    predicates.add(criteriaBuilder.lessThanOrEqualTo(visitDate, getStartOrEndOfDate(date, false)));
    query.where(criteriaBuilder.and(predicates.toArray(new Predicate[0])));

    return entityManager.createQuery(query).getResultList();
  }

  private Date getStartOrEndOfDate(LocalDate date, boolean startOfDate) {
    if (startOfDate) {
      return Date.from(date.atStartOfDay(DEFAULT_ZONE_ID).toInstant());
    }
    return Date.from(date.atTime(23, 59).atZone(DEFAULT_ZONE_ID).toInstant());
  }

}
