package com.cgi.dentistapp.repository;

import com.cgi.dentistapp.dto.SearchFilterDto;
import com.cgi.dentistapp.entity.DentistVisit;

import java.time.LocalDate;
import java.util.List;

public interface DentistVisitFinder {

  List<DentistVisit> findVisits(SearchFilterDto dto);
  List<DentistVisit> findVisits(Long dentistId, LocalDate date);
}
