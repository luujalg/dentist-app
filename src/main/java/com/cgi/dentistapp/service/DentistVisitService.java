package com.cgi.dentistapp.service;

import com.cgi.dentistapp.dto.DentistVisitDto;
import com.cgi.dentistapp.dto.SearchFilterDto;
import com.cgi.dentistapp.entity.DentistVisit;
import com.cgi.dentistapp.repository.DentistVisitDao;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static java.util.stream.Collectors.toList;

@Service
@Transactional
@RequiredArgsConstructor
public class DentistVisitService {

    public static final LocalTime TIME_OF_FIRST_ALLOWED_VISIT = LocalTime.of(10, 0);
    private final LocalTime TIME_OF_LAST_ALLOWED_VISIT = LocalTime.of(17, 0);
    public static final int VISIT_TIME_DURATION = 30;
    public static final int HOURS_BEFORE_VISIT_TO_ALLOW_EDIT_DELETE = 2;
    public static final int HOURS_FROM_NOW_TO_ADD_NEW_VISIT = 1;
    private final ZoneId DEFAULT_ZONE_ID = ZoneId.systemDefault();

    private final Logger LOGGER = LoggerFactory.getLogger(DentistVisitService.class);
    private final Comparator<DentistVisitDto> visitComparator = createComparator();
    private final DentistVisitDao dentistVisitDao;

    public void addVisit(DentistVisitDto dto) {
        LocalDateTime dateTime = LocalDateTime.of(dto.getDate(), dto.getTime());
        Date date = Date.from(dateTime.atZone(DEFAULT_ZONE_ID).toInstant());
        DentistVisit visit = new DentistVisit(dto.getDentistId(), date);
        LOGGER.info(String.format("Saving dentist visit data: %s", visit));
        dentistVisitDao.save(visit);
    }

    public List<DentistVisitDto> getAllRegistrations() {
        return StreamSupport.stream(dentistVisitDao.findAll().spliterator(), false)
            .map(this::createDto)
            .sorted(visitComparator)
            .collect(toList());
    }

    public void deleteRegistration(Long id) {
        LOGGER.info(String.format("Deleting registration with ID: %d", id));
        dentistVisitDao.delete(id);
    }

    public DentistVisitDto findVisit(Long id) {
        return createDto(dentistVisitDao.findOne(id));
    }

    private DentistVisitDto createDto(DentistVisit visit) {
        LOGGER.debug(String.format("Creating DTO for dentist visit: %s", visit));
        if (visit == null) {
            return null;
        }

        ZonedDateTime dateTime = visit.getVisitDate()
            .toInstant()
            .atZone(DEFAULT_ZONE_ID);

        boolean isEditable = dateTime.isAfter(ZonedDateTime.now(DEFAULT_ZONE_ID)
            .plusHours(HOURS_BEFORE_VISIT_TO_ALLOW_EDIT_DELETE));

        return DentistVisitDto.builder()
            .id(visit.getId())
            .dentistId(visit.getDentistId())
            .date(dateTime.toLocalDate())
            .time(dateTime.toLocalTime())
            .editable(isEditable)
            .visitDate(visit.getVisitDate())
            .build();
    }

    public void updateVisit(DentistVisitDto dto) {
        DentistVisit visit = dentistVisitDao.findOne(dto.getId());
        if (visit != null) {
            LOGGER.info(String.format("Updating registration data: %s", visit));
            LocalDateTime dateTime = LocalDateTime.of(dto.getDate(), dto.getTime());
            Date date = Date.from(dateTime.atZone(DEFAULT_ZONE_ID).toInstant());
            visit.setDentistId(dto.getDentistId());
            visit.setVisitDate(date);
            dentistVisitDao.save(visit);
        }
    }

    public List<DentistVisitDto> findVisits(SearchFilterDto searchFilterDto) {
        LOGGER.debug(String.format("Finding registrations: %s", searchFilterDto));
        return dentistVisitDao.findVisits(searchFilterDto).stream()
            .map(this::createDto)
            .sorted(visitComparator)
            .collect(toList());
    }

    public List<LocalTime> findAvailableTimes(long dentistId, LocalDate date) {
        LOGGER.debug(String.format("Finding available times: [dentistId=%d, date=%s]", dentistId, date));
        List<LocalTime> availableTimes = generateAvailableTimes(getRegistrationTimes(dentistId, date));
        if (date != null && LocalDate.now().isEqual(date)) {
            availableTimes = availableTimes.stream()
                .filter(time -> LocalTime.now().plusHours(HOURS_FROM_NOW_TO_ADD_NEW_VISIT).isBefore(time))
                .collect(toList());
        }
        return availableTimes;
    }

    private Set<LocalTime> getRegistrationTimes(long dentistId, LocalDate date) {
        List<DentistVisit> registrations = dentistVisitDao.findVisits(dentistId, date);
        return registrations.stream()
            .map(registration -> {
                Date visitDate = registration.getVisitDate();
                return LocalDateTime.ofInstant(visitDate.toInstant(), DEFAULT_ZONE_ID).toLocalTime();
            })
            .collect(Collectors.toSet());
    }

    private List<LocalTime> generateAvailableTimes(Set<LocalTime> registrationTimes) {
        LocalTime time = TIME_OF_FIRST_ALLOWED_VISIT;
        List<LocalTime> availableTimes = new ArrayList<>();
        while (!time.equals(TIME_OF_LAST_ALLOWED_VISIT)) {
            if (!registrationTimes.contains(time)) {
                availableTimes.add(time);
            }
            time = time.plusMinutes(VISIT_TIME_DURATION);
        }

        return availableTimes.stream()
            .sorted()
            .collect(toList());
    }

    private Comparator<DentistVisitDto> createComparator() {
        return (firstVisit, secondVisit) -> {
            LocalDateTime firstDateTime = LocalDateTime.of(firstVisit.getDate(), firstVisit.getTime());
            LocalDateTime secondDateTime = LocalDateTime.of(secondVisit.getDate(), secondVisit.getTime());
            return firstDateTime.compareTo(secondDateTime);
        };
    }
}
