package com.cgi.dentistapp.service;

import com.cgi.dentistapp.dto.DentistVisitDto;
import com.cgi.dentistapp.validation.*;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
@AllArgsConstructor
public class DentistVisitValidationService {

  private final DentistService dentistService;
  private final DentistVisitService dentistVisitService;

  private final Logger LOGGER = LoggerFactory.getLogger(DentistVisitValidationService.class);

  private List<ValidationRule> getCommonValidators() {
    return Arrays.asList(new HasValidDate(dentistVisitService), new HasValidDentist(dentistService));
  }

  public ValidationResult performPreSaveValidation(DentistVisitDto dto) {
    LOGGER.debug(String.format("Performing pre-save validation: %s", dto));
    List<ValidationRule> commonValidators = getCommonValidators();
    ValidationResult validationResult = new ValidationResult();
    commonValidators.forEach(validator -> validator.apply(dto, validationResult));
    return validationResult;
  }

  public ValidationResult performOnSaveValidation(DentistVisitDto dto) {
    LOGGER.debug(String.format("Performing on-save validation: %s", dto));
    List<ValidationRule> commonValidators = getCommonValidators();
    ValidationRule onSaveValidator = new HasValidVisitTime(dentistVisitService, dentistService);
    ValidationResult validationResult = new ValidationResult();

    commonValidators.forEach(validator -> validator.apply(dto, validationResult));
    if (!validationResult.hasErrors()) {
      onSaveValidator.apply(dto, validationResult);
    }

    return validationResult;
  }

  public boolean isEditable(Long id) {
    DentistVisitDto visit = dentistVisitService.findVisit(id);
    return visit != null && visit.isEditable();
  }

}
